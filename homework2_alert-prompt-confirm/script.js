let username = 'Type your name',
    userAge;

const validateData = () => {

    while ( typeof username !== 'string' || username == '' || isNaN(userAge) || userAge == '' ) {
        username = prompt("Type your name", `${username}`);
        userAge = +prompt("Type your age", '18');
    }
}

const checkUserAge = (name, age) => {

    let message;

    if (age < 18) {
        message = alert('You are not allowed to visit this website');
    } else if (age >= 18 && age <= 22) {
        message = confirm('Are you sure you want to continue?');
    } else {
        message = alert(`Welcome ${name}`)
    }

    return message
}

function initialize () {
    validateData();
    checkUserAge(username, userAge)
}

initialize();
