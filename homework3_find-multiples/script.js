let num = +prompt('Type a num');
for (let i = 1; i <= num; i++) {
        if(i % 5 === 0) console.log(i);
    }

let start,
    end;

const validateNums = () => {

    while ( !Number.isInteger(start) || start == '' || !Number.isInteger(end) || end =='' || start >= end ) {
        start = +prompt("Type number");
        end = +prompt("Type second number");
    }
}

function checkPrime(num) {
    if (isNaN(num) || !isFinite(num) || num % 1 || num < 2) return false;
    let s = Math.sqrt(num);
    for (let i = 2; i <= s; i++)
        if (num % i == 0) return false;
    return true;
}

const getNums = (first, second) => {
    for (let i = first; i < second; i++) {
        if(checkPrime(i)) {console.log(i)}
    }
}

function initialize () {
    validateNums();
    getNums(start, end)
}

initialize();
