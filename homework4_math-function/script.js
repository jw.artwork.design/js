let firstNum = 'Type first number',
    secondNum = 'Type second number',
    mathAction;

const validateNums = () => {

    while ( !Number.isInteger(firstNum) || firstNum == '' || !Number.isInteger(secondNum) || secondNum =='' ) {
        firstNum = +prompt("Type first number", `${firstNum}`);
        secondNum = +prompt("Type second number", `${secondNum}`);
        mathAction = prompt("Type +, -, / or *");
    }
}

const getResult = (first, second, action) => {

    switch (action) {
        case '+':
            console.log(first + second);
            break;

        case '-':
            console.log(first - second);
            break;

        case '/':
            console.log(first / second);
            break;

        case '*':
            console.log(first * second);
            break;
    }

}

function initialize () {

    validateNums();
    getResult(firstNum, secondNum, mathAction)

}

initialize();

